#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <cstdlib>
#include <mpi.h>

/**
  * Smaller value seems to have some stability issues.
  */
#define MPI_BUFFER_SIZE 32

using std::string;
using std::ifstream;
using std::cout;
using std::cerr;

#define BLOCK_LOW(id,p,n) ((id)*(n)/(p))
#define BLOCK_HIGH(id,p,n) (BLOCK_LOW((id)+1,p,n)-1)
#define BLOCK_SIZE(id,p,n) (BLOCK_HIGH(id,p,n)-BLOCK_LOW(id,p,n)+1)
#define BLOCK_OWNER(index,p,n) (((p)*((index)+1)-1)/(n))

/**
 * @brief The cost function slow down the calculation so our program doesn't run too fast
 * @param x
 * @return
 */
short cost(int x)
{
    int i, n_iter = 20;
    double dcost = 0;
    for (i = 0; i < n_iter; i++)
        dcost += pow(sin((double)x), 2) + pow(cos((double)x), 2);
    return (short)(dcost / n_iter + 0.1);
}


int** build_matrix(const char* firstString, int m, const char* secondString, int n, int* top, int* left, int world_size, int world_rank)
{
    int receive_buffer[MPI_BUFFER_SIZE];
    int send_buffer[MPI_BUFFER_SIZE];
    int send_tag = 1;
    int receive_tag = 1;

    //Allocate and build the tracing table
    int** c = new int*[m + 1];
    c[0] = new int[n + 1];
    memcpy(c[0], top, (n + 1) * sizeof(int));

    for (int i = 1; i <= m; i++) {
        c[i] = new int[n + 1];

        if (world_rank == 0) {
            c[i][0] = left[i];
        } else {
            if (i % MPI_BUFFER_SIZE == 1) {
                MPI_Recv(&receive_buffer, MPI_BUFFER_SIZE, MPI_INT, world_rank - 1, receive_tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                receive_tag++;
            }
            c[i][0] = receive_buffer[(i-1) % MPI_BUFFER_SIZE];
        }

        for (int j = 1; j <= n; j++) {
            if (firstString[i - 1] == secondString[j - 1]) {
                c[i][j] = c[i - 1][j - 1] + cost(i);
            } else {
                c[i][j] = c[i][j - 1] > c[i - 1][j] ? c[i][j - 1] : c[i - 1][j];
            }
        }

        send_buffer[(i - 1) % MPI_BUFFER_SIZE] = c[i][n];
        if (world_rank != world_size - 1) {
            if (i % MPI_BUFFER_SIZE == 0 || i == m) {
                MPI_Send(&send_buffer, MPI_BUFFER_SIZE, MPI_INT, world_rank + 1, send_tag, MPI_COMM_WORLD);
                send_tag++;
            }
        }
    }

    return c;
}

string back_track(int** c, const char* firstString, const char* secondString, int& i, int& j)
{
    string result = "";
    while (i >= 1 && j >= 1) {
        if (firstString[i - 1] == secondString[j - 1]) {
            result = firstString[i - 1] + result;
            i--;
            j--;
        } else if (c[i - 1][j] > c[i][j - 1]) {
            i--;
        } else {
            j--;
        }
    }

    return result;
}

string lcs(const char* firstString, const char* secondString, int m, int n, int world_size, int world_rank)
{
    int* left = new int[m + 1];
    int* top = new int[n + 1];

    memset(top, 0, sizeof(int) * (n + 1));
    memset(left, 0, sizeof(int) * (m + 1));
    int** c = build_matrix(firstString, m, secondString, n, top, left, world_size, world_rank);

    // Wait till all the matrix have been calculated before backtracking.
    MPI_Barrier(MPI_COMM_WORLD);

    //Backtrack
    // Wait for the result from the next process
    string result;
    if (world_rank != world_size - 1) {
        MPI_Recv(&m, 1, MPI_INT, world_rank + 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

        MPI_Status status;
        MPI_Probe(world_rank + 1, 1, MPI_COMM_WORLD, &status);
        int result_length;
        MPI_Get_count(&status, MPI_CHAR, &result_length);

        char* tmp = new char[result_length];
        MPI_Recv(tmp, result_length, MPI_CHAR, world_rank + 1, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

        result.assign(tmp, result_length);

        delete[] tmp;
    }

    //Since back_track will change the value of m, we need to stored it in order to release memory later
    int m_copy = m;
    result = back_track(c, firstString, secondString, m, n) + result;

    //Forward the current position and the result back to the previous process to merge
    if (world_rank != 0) {
        MPI_Send(&m, 1, MPI_INT, world_rank - 1, 0, MPI_COMM_WORLD);

        char* tmp = const_cast<char*>(result.c_str());
        MPI_Send(tmp, result.length(), MPI_CHAR, world_rank - 1, 1, MPI_COMM_WORLD);
    }

    //Deallocate and restore memory
    for (int i = 0; i <= m_copy; i++) {
        delete[] c[i];
    }
    delete[] c;
    delete[] top;
    delete[] left;

    return result;
}

#ifndef TEST_LCS_MPI_TEST
int main(int argc, char* argv[])
{
    if (argc != 2) {
        cout << "You need to supply one argument to this program.\n";
        cout << "Example: " << argv[0] << " <input-file>\n";
        return -1;
    }

    // Initialize the MPI environment
    MPI_Init(&argc, &argv);

    // Get the number of processes
    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    // Get the rank of the process
    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

#ifdef LCS_PROFILE
    double start_time;
    if (world_rank == 0) {
        start_time = MPI_Wtime();
    }
#endif

    // Read and broadcast the string size to every other process
    int size1, size2, partial2;
    ifstream inFile;
    if (world_rank == 0) {
        inFile.open(argv[1]);
        if (!inFile.is_open()) {
            cout << "Unable to open input file: " << argv[1] << "\n";
            return -2;
        }

        inFile >> size1 >> size2;
    }
    MPI_Bcast(&size1, 1, MPI_INTEGER, 0, MPI_COMM_WORLD);
    MPI_Bcast(&size2, 1, MPI_INTEGER, 0, MPI_COMM_WORLD);
    // We will break the second string into chunk with size partial
    partial2 = BLOCK_SIZE(world_rank, world_size, size2);

    char* firstString = new char[size1];
    char* secondString = new char[partial2];

    // Read and broadcast the first string to every other processes
    if (world_rank == 0) {
        inFile.ignore(size1, '\n');
        inFile.read(firstString, size1);
    }
    MPI_Bcast(firstString, size1, MPI_CHAR, 0, MPI_COMM_WORLD);

    // Read the second string, break them into chunk and send them to every other processes
    if (world_rank == 0) {
        int i, size;
        // Call this to get to the next line
        inFile.ignore(size1, '\n');

        char* read_buffer = new char[BLOCK_SIZE(world_size - 1, world_size, size2)];
        for (i = 0; i < world_size; i++) {
            size = BLOCK_SIZE(i, world_size, size2);
            if (i != 0) {
                inFile.read(read_buffer, size);
                MPI_Send(read_buffer, size, MPI_CHAR, i, 0, MPI_COMM_WORLD);
            } else {
                inFile.read(secondString, size);
            }
        }
        delete[] read_buffer;
    } else {
        MPI_Recv(secondString, partial2, MPI_CHAR, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }

    string output = lcs(firstString, secondString, size1, partial2, world_size, world_rank);

    if (world_rank == 0) {
        cout << output.length() << "\n";
        cout << output << "\n";
    }

    // Cleanup temporary resource for the reading process
    if (world_rank == 0) {
        inFile.close();
    }

    // Cleanup resource
    delete[] firstString;
    delete[] secondString;

#ifdef LCS_PROFILE
    if (world_rank == 0) {
        cerr << "Elapse time: " << MPI_Wtime() - start_time << " seconds \n";
    }
#endif

    MPI_Finalize();


    return 0;
}
#endif
