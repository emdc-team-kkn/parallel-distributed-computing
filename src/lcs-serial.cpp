#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <cstdlib>

#define CHOWBASE 16384

using std::string;
using std::ifstream;
using std::cout;

/**
 * @brief The cost function slow down the calculation so our program doesn't run too fast
 * @param x
 * @return
 */
short cost(int x)
{
    int i, n_iter = 20;
    double dcost = 0;
    for (i = 0; i < n_iter; i++)
        dcost += pow(sin((double)x), 2) + pow(cos((double)x), 2);
    return (short)(dcost / n_iter + 0.1);
}

int** build_matrix(const char* firstString, int m, const char* secondString, int n, int* top, int* left)
{
    //Allocate and build the tracing table
    int** c = new int* [m + 1];
    c[0] = new int[n + 1];
    memcpy(c[0], top, (n + 1) * sizeof(int));
    for (int i = 1; i <= m; i++) {
        c[i] = new int[n + 1];
        c[i][0] = left[i];
        for (int j = 1; j <= n; j++) {
            if (firstString[i - 1] == secondString[j - 1]) {
                c[i][j] = c[i - 1][j - 1] + cost(i);
            } else {
                c[i][j] = c[i][j - 1] > c[i - 1][j] ? c[i][j - 1] : c[i - 1][j];
            }
        }
    }
    return c;
}

string back_track(int** c, const char* firstString, const char* secondString, int& i, int& j)
{
    string result = "";
    while (i >= 1 && j >= 1) {
        if (firstString[i - 1] == secondString[j - 1]) {
            result = firstString[i - 1] + result;
            i--;
            j--;
        } else if (c[i - 1][j] > c[i][j - 1]) {
            i--;
        } else {
            j--;
        }
    }
    return result;
}

/**
 * @brief lcs Classic DP algorithm for the Longest Common Subsequence Problem
 * @param firstString The first string
 * @param secondString The second string
 * @return the longest common subsequence between firstString and secondString
 */
string lcs(string firstString, string secondString)
{
    int m = firstString.length(), n = secondString.length();
    int* left = new int[m + 1];
    int* top = new int[n + 1];
    memset(left, 0, (m + 1) * sizeof(int));
    memset(top, 0, (n + 1) * sizeof(int));

    int** c = build_matrix(firstString.c_str(), m, secondString.c_str(), n, top, left);

    //Since back_track will change the value of m, we need to stored it in order to release memory later
    int m_copy = m;
    string result = back_track(c, firstString.c_str(), secondString.c_str(), m, n);

    //Deallocate and restore memory
    for (int i = 0; i <= m_copy; i++) {
        delete[] c[i];
    }
    delete[] c;
    delete[] top;
    delete[] left;

    return result;
}

/**
  * Represent the output boundary of the lcs_output_boundary procedure
  */
typedef struct {
    int* bottom;
    int* right;
} BOUND;

/**
 * @brief lcs_output_boundary Implement output boundary procedure for Chowhudry Algorithm
 * @param firstString The first string
 * @param m The length of the first string
 * @param secondString The second string
 * @param n The length of the second string
 * @param top The top input boundary
 * @param left The left input boundary
 * @param base If the sub-matrix size get smaller than this value, we stop recursive search.
 * @return The pair bottom and right output boundary
 */
BOUND* lcs_output_boundary(const char* firstString, int m, const char* secondString, int n, int* top, int* left, int base)
{
    int middleM = m / 2;
    int middleN = n / 2;

    BOUND* ret = new BOUND;
    ret->bottom = new int[n + 1];
    ret->right = new int[m + 1];

    int maxMN = m > n ? m : n;
    if (maxMN <= base) {
        //Allocate and build the tracing table
        int** c = build_matrix(firstString, m, secondString, n, top, left);
        for (int i = 0; i <= m; i++) {
            ret->right[i] = c[i][n];
        }
        memcpy(ret->bottom, c[m], sizeof(int) * (n + 1));

        //Deallocate and restore memory
        for (int i = 0; i <= m; i++) {
            delete[] c[i];
        }
        delete[] c;
    } else {
        BOUND* b11 = lcs_output_boundary(
            firstString,
            middleM,
            secondString,
            middleN,
            top,
            left, base);
        BOUND* b12 = lcs_output_boundary(
            firstString,
            middleM,
            secondString + middleN,
            n - middleN,
            top + middleN,
            b11->right, base);
        BOUND* b21 = lcs_output_boundary(
            firstString + middleM,
            m - middleM,
            secondString,
            middleN,
            b11->bottom,
            left + middleM, base);
        BOUND* b22 = lcs_output_boundary(
            firstString + middleM,
            m - middleM,
            secondString + middleN,
            n - middleN,
            b12->bottom,
            b21->right, base);

        //Concat all the bottom together
        memcpy(ret->bottom, b21->bottom, sizeof(int) * (middleN + 1));
        memcpy(ret->bottom + middleN, b22->bottom, sizeof(int) * (n - middleN + 1));
        memcpy(ret->right, b12->right, sizeof(int) * (middleM + 1));
        memcpy(ret->right + middleM, b22->right, sizeof(int) * (m - middleM + 1));

        delete[] b11 -> bottom;
        delete[] b11 -> right;
        delete b11;

        delete[] b12 -> bottom;
        delete[] b12 -> right;
        delete b12;

        delete[] b21 -> bottom;
        delete[] b21 -> right;
        delete b21;

        delete[] b22 -> bottom;
        delete[] b22 -> right;
        delete b22;
    }

    return ret;
}

/**
 * @brief lcs_chowhudry_recursive Implement recursive search procedure for Chowhudry algorithm
 * @param firstString The first string
 * @param m The length of the first string
 * @param secondString The second string
 * @param n The length of the second string
 * @param top the top input boundary
 * @param left the left input boundary
 * @param i The current pointer position in the global search matrix
 * @param j The current pointer position in the global search matrix
 * @param offseti The offset of the current sub-matrix
 * @param offsetj The offset of the current sub-matrix
 * @param base If the sub-matrix size get smaller than this value, we stop recursive search.
 * @return The longest common subsequence between the first string and the second string
 */
string lcs_chowhudry_recursive(
    const char* firstString, int m,
    const char* secondString, int n,
    int* top, int* left,
    int& i, int& j,
    int offseti, int offsetj,
    int base)
{
    int middleM = m / 2;
    int middleN = n / 2;
    string result = "";

    int maxMN = m > n ? m : n;
    if (maxMN <= base) {
        //Allocate and build the tracing table
        int** c = build_matrix(firstString, m, secondString, n, top, left);

        //Backtrack
        int k = i - offseti, l = j - offsetj;
        result = back_track(c, firstString, secondString, k, l);
        i = k + offseti;
        j = l + offsetj;

        //Deallocate and restore memory
        for (int k = 0; k <= m; k++) {
            delete[] c[k];
        }
        delete[] c;
    } else {
        BOUND* b11 = lcs_output_boundary(
            firstString,
            middleM,
            secondString,
            middleN,
            top,
            left, base);
        //Bottom right
        if (i - offseti > middleM && j - offsetj > middleN) {
            BOUND* b12 = lcs_output_boundary(
                firstString,
                middleM,
                secondString + middleN,
                n - middleN,
                top + middleN,
                b11->right, base);
            BOUND* b21 = lcs_output_boundary(
                firstString + middleM,
                m - middleM,
                secondString,
                middleN,
                b11->bottom,
                left + middleM, base);
            result = lcs_chowhudry_recursive(
                         firstString + middleM,
                         m - middleM,
                         secondString + middleN,
                         n - middleN,
                         b12->bottom,
                         b21->right,
                         i,
                         j,
                         offseti + middleM,
                         offsetj + middleN, base) + result;

            delete[] b12 -> bottom;
            delete[] b12 -> right;
            delete b12;

            delete[] b21 -> bottom;
            delete[] b21 -> right;
            delete b21;
        }
        //Bottom left
        if (i - offseti > middleM && j - offsetj <= middleN && j - offsetj > 0) {
            result = lcs_chowhudry_recursive(
                         firstString + middleM,
                         m - middleM,
                         secondString,
                         middleN,
                         b11->bottom,
                         left + middleM,
                         i,
                         j,
                         offseti + middleM,
                         offsetj, base) + result;
        }
        //Top right
        if (i - offseti <= middleM && i - offseti > 0 && j - offsetj > middleN) {
            result = lcs_chowhudry_recursive(
                         firstString,
                         middleM,
                         secondString + middleN,
                         n - middleN,
                         top + middleN,
                         b11->right,
                         i,
                         j,
                         offseti,
                         offsetj + middleN, base) + result;
        }
        if (i - offseti > 0 && i - offseti <= middleM && j - offsetj > 0 && j - offsetj <= middleN) {
            result = lcs_chowhudry_recursive(
                         firstString,
                         middleM,
                         secondString,
                         middleN,
                         top,
                         left,
                         i,
                         j,
                         offseti, offsetj, base) + result;
        }
        delete[] b11 -> bottom;
        delete[] b11 -> right;
        delete b11;
    }

    return result;
}

/**
 * @brief lcs_chowhudry Wrapper for Chowhudry algorithm to accept the same parameter as the classic algorithm
 * @param firstString The first string
 * @param secondString The second string
 * @return The longest common subsequence between first string and second string
 */
string lcs_chowhudry(string firstString, string secondString)
{
    int m = firstString.length(), n = secondString.length();
    int base = CHOWBASE;
    int* left = new int[m + 1];
    int* top = new int[n + 1];
    memset(left, 0, (m + 1) * sizeof(int));
    memset(top, 0, (n + 1) * sizeof(int));

    string result = lcs_chowhudry_recursive(firstString.c_str(), m, secondString.c_str(), n, top, left, m, n, 0, 0, base);

    delete[] top;
    delete[] left;
    return result;
}

#ifndef TEST_LCS_SERIAL_TEST
int main(int argc, char* argv[])
{
    if (argc != 2) {
        cout << "You need to supply one argument to this program.\n";
        cout << "Example: " << argv[0] << " <input-file>\n";
        return -1;
    }

    ifstream inFile(argv[1]);
    if (!inFile.is_open()) {
        cout << "Unable to open input file\n";
        return -2;
    }

    int size1, size2;
    string firstString, secondString;
    inFile >> size1 >> size2 >> firstString >> secondString;
    if (inFile.fail()) {
        cout << "Invalid input format.\n";
        inFile.close();
        return -3;
    }

    string output;
    char* lowMemory = getenv ("LCS_LOW_MEMORY");
    if(lowMemory != NULL) {
        output = lcs_chowhudry(firstString, secondString);
    } else {
        output = lcs(firstString, secondString);
    }

    cout << output.length() << "\n";
    cout << output << "\n";

    inFile.close();
    return 0;
}
#endif
