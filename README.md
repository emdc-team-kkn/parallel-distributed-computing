# Requirement

* Cmake >= 2.8

# How to build

    cd build
    cmake .. # Only to run once to generate the Makefile or if the make command return an error
    make # Run the make file to compile

After build, the executable will be in `build/src` and `build/test` directory 

# How to run test

    cd build
    cmake ..  # Only to run once to generate the Makefile or if the make command return an error
    make test-all # Run all test
    make test-serial # Run test for serial implementation
    make test-omp # Run test for OpenMP implementation
    make test-mpi # Run test for MPI implementation

# How to write test

The testing library is gtest-1.7 from Google.
Documents can be found at 

* [Basic guide][1]
* [Complete Reference][2]


[1]:https://code.google.com/p/googletest/wiki/V1_7_Primer
[2]:https://code.google.com/p/googletest/w/list



