#ifndef TEST_OUTPUT_FILE_H
#define TEST_OUTPUT_FILE_H

#include<string>

using std::string;

namespace utils {

class OutputFile
{
public:
    int size;
    string sequence;
    OutputFile();
    ~OutputFile();
    bool Read(const char* filePath);
    bool Write(const char* filePath);
};

}

#endif // OUTPUTFILE_H
