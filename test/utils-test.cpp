#include <gtest/gtest.h>
#include "input-file.h"
#include "output-file.h"

using utils::InputFile;
using utils::OutputFile;

TEST(Utils, ReadInput) {
    InputFile inputFile;
    bool readSuccess = inputFile.Read("data/ex10.15.in");

    ASSERT_TRUE(readSuccess);
    ASSERT_EQ(10, inputFile.size1);
    ASSERT_EQ(15, inputFile.size2);
    ASSERT_EQ("GTCGCGTACC", inputFile.firstString);
    ASSERT_EQ("TGTGGTATAAGAGCT", inputFile.secondString);
}

TEST(Utils, ReadOutput) {
    OutputFile outputFile;
    bool readSuccess = outputFile.Read("data/ex10.15.out");

    ASSERT_TRUE(readSuccess);
    ASSERT_EQ("GTGGTAC", outputFile.sequence);
    ASSERT_EQ(7, outputFile.size);

}

TEST(Utils, WriteOutput) {
    OutputFile outputFile;
    outputFile.size = 7;
    outputFile.sequence = "ABC!@#$";
    bool writeSuccess = outputFile.Write("output.tmp");
    ASSERT_TRUE(writeSuccess);

    OutputFile outputFile2;
    bool readSuccess = outputFile2.Read("output.tmp");
    ASSERT_TRUE(readSuccess);
    ASSERT_EQ(outputFile.sequence, outputFile2.sequence);
    ASSERT_EQ(outputFile.size, outputFile2.size);
}
