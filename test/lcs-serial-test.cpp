#define TEST_LCS_SERIAL_TEST

#include <gtest/gtest.h>
#include <utility>
#include <string>
#include <iostream>
#include "input-file.h"
#include "output-file.h"
#include "lcs-serial.cpp"

using std::pair;
using std::string;
using utils::InputFile;
using utils::OutputFile;

class TestFixture : public ::testing::TestWithParam<pair<string, string> > {
};

TEST_P(TestFixture, LCS)
{
    pair<string, string> inputOutput = GetParam();

    InputFile inputFile;
    bool readInputFile = inputFile.Read(inputOutput.first.c_str());
    ASSERT_TRUE(readInputFile);

    OutputFile outputFile;
    bool readOutputFile = outputFile.Read(inputOutput.second.c_str());
    ASSERT_TRUE(readOutputFile);

    string commonSubsequence = lcs(inputFile.firstString, inputFile.secondString);
    ASSERT_EQ(outputFile.size, commonSubsequence.length());
    ASSERT_EQ(outputFile.sequence, commonSubsequence);
}

TEST_P(TestFixture, LCS_Chowhudry)
{
    pair<string, string> inputOutput = GetParam();

    InputFile inputFile;
    bool readInputFile = inputFile.Read(inputOutput.first.c_str());
    ASSERT_TRUE(readInputFile);

    OutputFile outputFile;
    bool readOutputFile = outputFile.Read(inputOutput.second.c_str());
    ASSERT_TRUE(readOutputFile);

    string commonSubsequence = lcs_chowhudry(inputFile.firstString, inputFile.secondString);
    ASSERT_EQ(outputFile.size, commonSubsequence.length());
    ASSERT_EQ(outputFile.sequence, commonSubsequence);
}

INSTANTIATE_TEST_CASE_P(
    FastSerialTest,
    TestFixture,
    ::testing::Values(
        pair<string, string>("data/ex10.15.in", "data/ex10.15.out"),
        pair<string, string>("data/ex150.200.in", "data/ex150.200.out"),
        pair<string, string>("data/ex3k.8k.in", "data/ex3k.8k.out"),
        pair<string, string>("data/ex2k.2k.in", "data/ex2k.2k.out")));

/**
 * The last two tests are already passed but are too slow to run everytime
 */
/*
INSTANTIATE_TEST_CASE_P(
    SlowSerialTest,
    TestFixture,
    ::testing::Values(
        pair<string, string>("data/ex18k.17k.in", "data/ex18k.17k.out"),
        pair<string, string>("data/ex35k.30k.in", "data/ex35k.30k.out"),
        pair<string, string>("data/ex48k.30k.in", "data/ex48k.30k.out")
    )
);
*/
