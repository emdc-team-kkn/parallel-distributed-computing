#include "output-file.h"
#include <fstream>

using std::ifstream;
using std::ofstream;

namespace utils
{

OutputFile::OutputFile()
{
}

OutputFile::~OutputFile()
{
}

bool OutputFile::Read(const char *filePath)
{
    ifstream inFile(filePath);
    if (!inFile.is_open()) {
        return false;
    }

    inFile >> size >> sequence;
    if (inFile.fail()) {
        inFile.close();
        return false;
    }

    return true;
}

bool OutputFile::Write(const char *filePath)
{
    ofstream outFile(filePath);
    if (!outFile.is_open()) {
        return false;
    }
    outFile << size << "\n";
    outFile << sequence;
    outFile.close();
    return true;
}

}
