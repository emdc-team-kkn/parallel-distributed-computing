#include "input-file.h"
#include <fstream>
#include <string>
#include <sstream>

using std::ifstream;
using std::ios;
using std::string;

namespace utils {

InputFile::InputFile()
{
}

InputFile::~InputFile()
{
}

bool InputFile::Read(const char* filePath)
{
    ifstream inFile(filePath);
    if (!inFile.is_open()) {
        return false;
    }

    inFile >> size1 >> size2 >> firstString >> secondString;
    if (inFile.fail()) {
        inFile.close();
        return false;
    }

    return true;
}

}
