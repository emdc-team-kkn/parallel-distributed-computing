#define TEST_LCS_MPI_TEST

#include <gtest/gtest.h>
#include <utility>
#include <string>
#include <iostream>
#include "input-file.h"
#include "output-file.h"
#include "lcs-mpi.cpp"

using std::pair;
using std::string;
using utils::InputFile;
using utils::OutputFile;

class TestFixture : public ::testing::TestWithParam<pair<string, string> > {
};

TEST_P(TestFixture, LCS)
{
    pair<string, string> inputOutput = GetParam();

    // Get the number of processes
    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    // Get the rank of the process
    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    // Read and broadcast the string size to every other process
    int size1, size2, partial2;
    ifstream inFile;
    if (world_rank == world_size - 1) {
        inFile.open(inputOutput.first.c_str());
        inFile >> size1 >> size2;
    }
    MPI_Bcast(&size1, 1, MPI_INTEGER, world_size - 1, MPI_COMM_WORLD);
    MPI_Bcast(&size2, 1, MPI_INTEGER, world_size - 1, MPI_COMM_WORLD);
    // We will break the second string into chunk with size partial
    partial2 = BLOCK_SIZE(world_rank, world_size, size2);

    char* firstString = new char[size1];
    char* secondString = new char[partial2];

    // Read and broadcast the first string to every other processes
    if (world_rank == world_size - 1) {
        inFile.ignore(size1, '\n');
        inFile.read(firstString, size1);
    }
    MPI_Bcast(firstString, size1, MPI_CHAR, world_size - 1, MPI_COMM_WORLD);

    // Read the second string, break them into chunk and send them to every other processes
    if (world_rank == world_size - 1) {
        int i, size;
        // Call this to get to the next line
        inFile.ignore(size1, '\n');

        for (i = 0; i < world_size; i++) {
            size = BLOCK_SIZE(i, world_size, size2);
            inFile.read(secondString, size);
            if (i != world_size - 1) {
                MPI_Send(secondString, size, MPI_CHAR, i, 0, MPI_COMM_WORLD);
            }
        }
    } else {
        MPI_Recv(secondString, partial2, MPI_CHAR, world_size - 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }

    string commonSubsequence = lcs(firstString, secondString, size1, partial2, world_size, world_rank);

    if (world_rank == 0) {
        OutputFile outputFile;
        bool readOutputFile = outputFile.Read(inputOutput.second.c_str());
        ASSERT_TRUE(readOutputFile);

        ASSERT_EQ(outputFile.size, commonSubsequence.length());
        ASSERT_EQ(outputFile.sequence, commonSubsequence);
    }
}

INSTANTIATE_TEST_CASE_P(
    FastMPITest,
    TestFixture,
    ::testing::Values(
        pair<string, string>("data/ex10.15.in", "data/ex10.15.out"),
        pair<string, string>("data/ex150.200.in", "data/ex150.200.out"),
        pair<string, string>("data/ex3k.8k.in", "data/ex3k.8k.out"),
        pair<string, string>("data/ex2k.2k.in", "data/ex2k.2k.out")));

/**
 * The last two tests are already passed but are too slow to run everytime
 */
/*
INSTANTIATE_TEST_CASE_P(
    SlowMPITest,
    TestFixture,
    ::testing::Values(
        pair<string, string>("data/ex18k.17k.in", "data/ex18k.17k.out"),
        pair<string, string>("data/ex35k.30k.in", "data/ex35k.30k.out"),
        pair<string, string>("data/ex48k.30k.in", "data/ex48k.30k.out")
    )
);
*/

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);

    // Initialize the MPI environment
    MPI_Init(&argc, &argv);
    int ret = RUN_ALL_TESTS();
    MPI_Finalize();
    return ret;
}
