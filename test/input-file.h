#ifndef TEST_INPUT_FILE_H
#define TEST_INPUT_FILE_H

#include <string>

using std::string;

namespace utils {

class InputFile {
public:
    int size1, size2;
    string firstString, secondString;

    InputFile();
    ~InputFile();
    bool Read(const char* filePath);
};

}

#endif
